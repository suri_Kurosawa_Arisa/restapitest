package models

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.ws._
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._

/**
 * add your integration spec here.
 * An integration test will fire up a whole play application in a real (or headless) browser
 */
class ADSLW0100Spec extends PlaySpec with OneServerPerTest with OneBrowserPerTest with HtmlUnitFactory {
  val url = "http://10.1.2.34/system/api/ADSLW0100"


  "ADSLW0100" should {

    "ADSLW0100GetReq json" in {
//      val getreq = ADSLW0100GetReq(
//          srchTermType = 1,
//          srchTermFrom = 2,
//          srchTermTo = 3,
//          srchUnitID = 4,
//          srchZoneID = 5,
//          srchAreaID = 6,
 //        srchStaffPostGrp = 7
  //       )
 //     val getreqjson = Json.toJson(getreq)
      val sjson:String =
        """
          {"reqHead":"reqHead",
          "reqPage": "reqPage",
          "srchTermType" :1,
          "srchTermFrom" :2,
          "srchTermTo" :3,
          "srchUnitID" :4,
          "srchZoneID" :5,
          "srchAreaID" :6,
          "srchStaffPostGrp": 7
          }
        """
        val sj = Json.parse(sjson)
   //     getreqjson mustBe sj
    }


      "ADSLW0100Rec json" in {
      val rec = ADSLW0100Rec (
        storeID = 1,
        storeCode = "storeCode",
        storeName = "storeName",
        staffID = 2,
        staffCode = "staffCode",
        staffName = "staffName",
        am = 3,
        qyAvg = 234.123,
        heavyCqy = 4,
        heavyCqyPrice = 5,
        heavyCprice = 6,
        mediumCqy = 7,
        mediumCqyPrice = 8,
        mediumCprice = 9)
      val sjson = """{
        "storeID": 1,
        "storeCode": "storeCode",
        "storeName": "storeName",
        "staffID": 2,
        "staffCode": "staffCode",
        "staffName": "staffName",
        "am": 3,
        "qyAvg": 234.123,
        "heavyCqy": 4,
        "heavyCqyPrice": 5,
        "heavyCprice": 6,
        "mediumCqy": 7,
        "mediumCqyPrice": 8,
        "mediumCprice": 9
        }"""
        val recjson = Json.toJson(rec)
        val sj = Json.parse(sjson)
        recjson mustBe sj
    }
      "ADSLW0100GetRsp json" in {
      val rec1 = ADSLW0100Rec (
        storeID = 1,
        storeCode = "storeCode",
        storeName = "storeName",
        staffID = 2,
        staffCode = "staffCode",
        staffName = "staffName",
        am = 3,
        qyAvg = 234.123,
        heavyCqy = 4,
        heavyCqyPrice = 5,
        heavyCprice = 6,
        mediumCqy = 7,
        mediumCqyPrice = 8,
        mediumCprice = 9)
      val rec2 = ADSLW0100Rec (
        storeID = 11,
        storeCode = "storeCode2",
        storeName = "storeName2",
        staffID = 12,
        staffCode = "staffCode2",
        staffName = "staffName2",
        am = 13,
        qyAvg = 234.123,
        heavyCqy = 14,
        heavyCqyPrice = 15,
        heavyCprice = 16,
        mediumCqy = 17,
        mediumCqyPrice = 18,
        mediumCprice = 19)
      //val getrsp = ADSLW0100GetRsp(
        //rspHead = "rspHead",
        //rspPage = "rspPage",
        //rspList = List(rec1, rec2)
        //  )
      val sjson = """{
        "rspHead": "rspHead",
        "rspPage": "rspPage",
        "rspList":[{"storeID": 1,
        "storeCode": "storeCode",
        "storeName": "storeName",
        "staffID": 2,
        "staffCode": "staffCode",
        "staffName": "staffName",
        "am": 3,
        "qyAvg": 234.123,
        "heavyCqy": 4,
        "heavyCqyPrice": 5,
        "heavyCprice": 6,
        "mediumCqy": 7,
        "mediumCqyPrice": 8,
        "mediumCprice": 9},
        {"storeID": 11,
        "storeCode": "storeCode2",
        "storeName": "storeName2",
        "staffID": 12,
        "staffCode": "staffCode2",
        "staffName": "staffName2",
        "am": 13,
        "qyAvg": 234.123,
        "heavyCqy": 14,
        "heavyCqyPrice": 15,
        "heavyCprice": 16,
        "mediumCqy": 17,
        "mediumCqyPrice": 18,
        "mediumCprice": 19}]
        }"""
        //val getrspjson = Json.toJson(getrsp)
        val sj = Json.parse(sjson)
      //  getrspjson mustBe sj
    }
  }
}

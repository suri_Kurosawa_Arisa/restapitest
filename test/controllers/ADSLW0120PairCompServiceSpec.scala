package controllers

import org.scalatestplus.play._
import org.scalatest.BeforeAndAfter
import play.api.test._
import play.api.test.Helpers._
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning._
//import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import play.api.libs.json.Json
import play.api.libs.json._
import java.text.ParseException
import scala.concurrent._
import scala.sys.process._
import java.io.PrintWriter
import models._

import play.api.libs.ws.ning.NingWSClient
import scala.concurrent.ExecutionContext.Implicits.global



/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ADSLW0120PairCompServiceSpec extends PlaySpec with BeforeAndAfter { //with OneAppPerTest {
//  implicit val context = scala.concurrent.ExecutionContext.Implicits.global

  before {
    Process("/home/aokmdb/work/kurosawa/ADSLW0120/test5/insert.sh") !!
  }

  after {
    Process("/home/aokmdb/work/kurosawa/ADSLW0120/test5/delete.sh") !!
  }

  /*
   * ここより試験を記述
   * should は大項目
   * in は小項目と考える
   */
  "ADSLW0120" should {
    // 外のスコープで定義した内容は中のスコープで使えるので複数試験で使う定数はここで定義する。
//    val url = "http://10.1.2.34/system/api/ADSLW0120";
 //   val url = "http://localhost:8880/system/api/ADSLW0120";
      val url = "http://10.1.2.34/system/api/ADSLW0120";

    "No.5計画達成ランキング・新店・昨日・経準率・出力区分エリア" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20050401,
          srchTermTo = 20050401,
          srchUnitID = 3,
          srchZoneID = 986,//神奈川第一ゾーン
          srchAreaID = 0,
          storeYearType = 3, //新店
          srchValType  = 3,//経準率
          srchOrgType = 3 // 店舗
	  )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 317,
        orgCode = "0139",
        orgName = "横浜弘明寺",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 317,
        orgCode = "0139",
        orgName = "横浜弘明寺",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val bestgetrsp = List(rec1)
      val worstgetrsp = List(rec2)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.6計画達成ランキング・昨日・新店舗・売上高・出力区分：新店抱合エリア" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1, //期間種別：日単位
          srchTermFrom = 20050401,
          srchTermTo = 20050401,
          srchUnitID = 3,
          srchZoneID = 986,  //神奈川第一ゾーン
          srchAreaID = 1040, //小濱哲エリア
          storeYearType = 3,//新店
          srchValType  = 1, //出力区分：売上高
          srchOrgType = 3)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 317,
        orgCode = "0139",
        orgName = "横浜弘明寺",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 317,
        orgCode = "0139",
        orgName = "横浜弘明寺",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)


      val bestgetrsp = List(rec1)
      val worstgetrsp = List(rec2)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }
    "No.7計画達成ランキング・昨日・既存店舗・売上高・出力区分：新店抱合エリア" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1, //期間種別：日単位
          srchTermFrom = 20050401,
          srchTermTo = 20050401,
          srchUnitID = 3,
          srchZoneID = 986, //神奈川第一ゾーン
          srchAreaID = 1040, //小濱哲ゾーン
          storeYearType = 2, //既存店
          srchValType  = 1, //出力区分：売上高
          srchOrgType = 3)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 240,
        orgCode = "0122",
        orgName = "横浜日野店",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 240,
        orgCode = "0122",
        orgName = "横浜日野店",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val bestgetrsp = List(rec1)
      val worstgetrsp = List(rec2)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.8計画達成ランキング・全店・昨日・売上高・出力区分：新店抱合エリア" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20050401,
          srchTermTo = 20050401,
          srchUnitID = 3,
          srchZoneID = 986, //神奈川第一ゾーン
          srchAreaID = 1040, //小濱哲エリア
          storeYearType = 1,//全店
          srchValType  = 1,//売上高
          srchOrgType = 3)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 240,
        orgCode = "0122",
        orgName = "横浜日野店",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 317,
        orgCode = "0139",
        orgName = "横浜弘明寺",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec3 = ADSLW0120Rec (
         orgID = 317,
         orgCode = "0139",
         orgName = "横浜弘明寺",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec4 = ADSLW0120Rec (
         orgID = 240,
         orgCode = "0122",
         orgName = "横浜日野店",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

      val bestgetrsp = List(rec1,rec2)
      val worstgetrsp = List(rec3,rec4)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }
  }
}

package controllers

import org.scalatestplus.play._
import org.scalatest.BeforeAndAfter
import play.api.test._
import play.api.test.Helpers._
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning._
//import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import play.api.libs.json.Json
import play.api.libs.json._
import java.text.ParseException
import scala.concurrent._
import scala.sys.process._
import java.io.PrintWriter
import models._

import play.api.libs.ws.ning.NingWSClient
import scala.concurrent.ExecutionContext.Implicits.global



/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ADACW0010ServiceSpec extends PlaySpec with BeforeAndAfter {
  before {
    Process("/home/aokmdb/work/kurosawa/ADACW0010/test3/insert.sh") !!
  }

  after {
    Process("/home/aokmdb/work/kurosawa/ADACW0010/test3/delete.sh") !!
  }

  /*
   * ここより試験を記述
   * should は大項目
   * in は小項目と考える
   */
  "ADACW0010" should {
    // 外のスコープで定義した内容は中のスコープで使えるので複数試験で使う定数はここで定義する。
    val url = "http://10.1.2.34/system/api/ADACW0010";
 //   val url = "http://localhost:8880/system/api/ADACW0010";
/*    class TestClient(ws: WSClient, baseUrl: String) {
      @Inject def this(ws: WSClient) = this(ws, url)
    }*/
    "No.3計画達成必要額・着地見込・当月・エリア絞り込み・既存店" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADACW0010GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 3,
          srchTermFrom = 200504,
          srchTermTo = 200504,
          srchUnitID = 3,
          srchZoneID = 986,
          srchAreaID = 1040,
          srchStoreID = 240
          )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADACW0010Rec (
        days = 29,
        daysBase = 30,
        amReq  = 90,
        amComp = 0.0,
        amSub = 90,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        bizProfit =10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

      val rec2 = ADACW0010Rec (
        days = 29,
        daysBase = 30,
        amReq  = 90,
        amComp = 0.0,
        amSub = 90,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        bizProfit =10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

       val rec3 = ADACW0010Rec (
        days = 29,
        daysBase = 30,
        amReq  = 0,
        amComp = 0.0,
        amSub = 0,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 0,
        amPlanComp = 0.0,
        amPlanSub = 0,
        profit = 0,
        profitPlanComp = 0.0,
        profitPlanSub = 0,
        bizProfit = 0,
        bizProfitPlanComp = 0.0,
        bizProfitPlanSub = 0
        )
       val getrsp = List(rec1,rec2,rec3)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADACW0010GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADACW0010GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.3-2計画達成必要額・着地見込・当月・全国(全国と比較中の値)" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADACW0010GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 3,
          srchTermFrom = 200504,
          srchTermTo = 200504,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStoreID = 0
          )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADACW0010Rec (
        days = 29,
        daysBase = 30,
        amReq  = 180,
        amComp = 0.0,
        amSub = 180,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 420,
        amPlanComp = 105.0,
        amPlanSub = 20,
        profit = 200,
        profitPlanComp = 13.3,
        profitPlanSub = -1300,
        bizProfit = 20,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

      val rec2 = ADACW0010Rec (
        days = 29,
        daysBase = 30,
        amReq  = 90,
        amComp = 0.0,
        amSub = 90,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        bizProfit =10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

       val rec3 = ADACW0010Rec (
        days = 29,
        daysBase = 30,
        amReq  = 90,
        amComp = 0.0,
        amSub = 90,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 20.0,
        profitPlanSub = -400,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )
       val getrsp = List(rec1,rec2,rec3)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADACW0010GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADACW0010GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.4計画達成必要額・着地見込・当半期・全エリア" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADACW0010GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 6,
          srchTermFrom = 20051,
          srchTermTo = 20051,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStoreID = 0
          )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADACW0010Rec (
        days = 182,
        daysBase = 183,
        amReq  = 180,
        amComp = 3.6,
        amSub = -4820,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 420,
        amPlanComp = 105.0,
        amPlanSub = 20,
        profit = 200,
        profitPlanComp = 6.7,
        profitPlanSub = -2800,
        bizProfit = 20,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

      val rec2 = ADACW0010Rec (
        days = 182,
        daysBase = 183,
        amReq  = 90,
        amComp = 3.6,
        amSub = -2410,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 5.0,
        profitPlanSub = -1900,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

       val rec3 = ADACW0010Rec (
        days = 182,
        daysBase = 183,
        amReq  = 90,
        amComp = 3.6,
        amSub = -2410,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )
       val getrsp = List(rec1,rec2,rec3)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADACW0010GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADACW0010GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.7計画達成必要額・着地見込・当半期・ゾーンエリア絞り込み・新店舗" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADACW0010GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 6,
          srchTermFrom = 20051,
          srchTermTo = 20051,
          srchUnitID = 3,
          srchZoneID = 986,
          srchAreaID = 1040,
          srchStoreID = 317
          )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADACW0010Rec (
        days = 182,
        daysBase = 183,
        amReq  = 90,
        amComp = 3.6,
        amSub = -2410,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

      val rec2 = ADACW0010Rec (
        days = 182,
        daysBase = 183,
        amReq  = 0,
        amComp = 0.0,
        amSub = 0,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 0,
        amPlanComp = 0.0,
        amPlanSub = 0,
        profit = 0,
        profitPlanComp = 0.0,
        profitPlanSub = 0,
        bizProfit = 0,
        bizProfitPlanComp = 0.0,
        bizProfitPlanSub = 0
        )

       val rec3 = ADACW0010Rec (
        days = 182,
        daysBase = 183,
        amReq  = 90,
        amComp = 3.6,
        amSub = -2410,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 210,
        amPlanComp = 105.0,
        amPlanSub = 10,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )
       val getrsp = List(rec1,rec2,rec3)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADACW0010GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADACW0010GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.8計画達成必要額・着地見込・当年・全国" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADACW0010GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 4,
          srchTermFrom = 2005,
          srchTermTo = 2005,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStoreID = 0
          )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADACW0010Rec (
        days = 364,
        daysBase = 365,
        amReq  = 380,
        amComp = 7.6,
        amSub = -4620,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 620,
        amPlanComp = 103.3,
        amPlanSub = 20,
        profit = 400,
        profitPlanComp = 13.3,
        profitPlanSub = -2600,
        bizProfit = 20,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

      val rec2 = ADACW0010Rec (
        days = 364,
        daysBase = 365,
        amReq  = 190,
        amComp = 7.6,
        amSub = -2310,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 310,
        amPlanComp = 103.3,
        amPlanSub = 10,
        profit = 200,
        profitPlanComp = 10.0,
        profitPlanSub = -1800,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )

       val rec3 = ADACW0010Rec (
        days = 364,
        daysBase = 365,
        amReq  = 190,
        amComp = 7.6,
        amSub = -2310,
        bizProfitReq = 0,
        bizProfitComp = 0.0,
        bizProfitSub = 0,
        am = 310,
        amPlanComp = 103.3,
        amPlanSub = 10,
        profit = 200,
        profitPlanComp = 20.0,
        profitPlanSub = -800,
        bizProfit = 10,
        bizProfitPlanComp = 100.0,
        bizProfitPlanSub = 0
        )
       val getrsp = List(rec1,rec2,rec3)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADACW0010GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADACW0010GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }
  }

}

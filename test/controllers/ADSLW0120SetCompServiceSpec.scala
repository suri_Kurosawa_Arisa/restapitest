package controllers

import org.scalatestplus.play._
import org.scalatest.BeforeAndAfter
import play.api.test._
import play.api.test.Helpers._
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning._
//import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import play.api.libs.json.Json
import play.api.libs.json._
import java.text.ParseException
import scala.concurrent._
import scala.sys.process._
import java.io.PrintWriter
import models._

import play.api.libs.ws.ning.NingWSClient
import scala.concurrent.ExecutionContext.Implicits.global



/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ADSLW0120SetCompServiceSpec extends PlaySpec with BeforeAndAfter {

  before {
    Process("/home/aokmdb/work/kurosawa/ADSLW0120/test1/insert.sh") !!
  }

  after {
    Process("/home/aokmdb/work/kurosawa/ADSLW0120/test1/delete.sh") !!
  }

  /*
   * ここより試験を記述
   * should は大項目
   * in は小項目と考える
   */
  "ADSLW0120" should {
    // 外のスコープで定義した内容は中のスコープで使えるので複数試験で使う定数はここで定義する。
//    val url = "http://10.1.2.34/system/api/ADSLW0120";
 //   val url = "http://localhost:8880/system/api/ADSLW0120";
      val url = "http://10.1.2.34/system/api/ADSLW0120";

    "No.1計画達成ランキング・全店舗・本日・売上高・出力区分ゾーン" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20050402,
          srchTermTo = 20050402,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          storeYearType =1,
          srchValType  = 1,
          srchOrgType = 1)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 981,
        orgCode = "AA",
        orgName = "東京都心",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 986,
        orgCode = "AE",
        orgName = "神奈川第一",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec3 = ADSLW0120Rec (
         orgID = 988,
         orgCode = "AF",
         orgName = "神奈川第二",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec4 = ADSLW0120Rec (
         orgID = 991,
         orgCode = "AG",
         orgName = "千葉第一",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec5 = ADSLW0120Rec (
         orgID = 993,
         orgCode = "AH",
         orgName = "千葉第二",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec6 = ADSLW0120Rec (
        orgID = 990,
        orgCode = "BD",
        orgName = "中京第二",
        am = 100,
        planComp = 4.0,
        planSub = -2400,
        lastComp = 0.0,
        lastSub = 100,
        profit = 90,
        profitPlanComp = 9.0,
        profitPlanSub = -910,
        profitLastComp = 0.0,
        profitLastSub = 90,
        profitRt = 90.0,
        profitRtPlanSub = 50.0,
        profitRtLastSub = 90.0)

       val rec7 = ADSLW0120Rec (
        orgID = 993,
        orgCode = "AH",
        orgName = "千葉第二",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec8 = ADSLW0120Rec (
        orgID = 991,
        orgCode = "AG",
        orgName = "千葉第一",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec9 = ADSLW0120Rec (
         orgID = 988,
         orgCode = "AF",
         orgName = "神奈川第二",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec10 = ADSLW0120Rec (
         orgID = 986,
         orgCode = "AE",
         orgName = "神奈川第一",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

      val bestgetrsp = List(rec1,rec2,rec3,rec4,rec5)
      val worstgetrsp = List(rec6,rec7,rec8,rec9,rec10)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.2計画達成ランキング・本日・既存店舗・経準高・出力区分エリア" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1, //期間種別：日単位
          srchTermFrom = 20050402,
          srchTermTo = 20050402,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          storeYearType =2, //既存店
          srchValType  = 2, //出力区分：経準高
          srchOrgType =2)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 1014,
        orgCode = "AAA",
        orgName = "吉田　宏明",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 1043,
        orgCode = "AEC",
        orgName = "澤田　健二",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec3 = ADSLW0120Rec (
         orgID = 1046,
         orgCode = "AFA",
         orgName = "柳沢　公晴",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec4 = ADSLW0120Rec (
         orgID = 1058,
         orgCode = "AGA",
         orgName = "伊藤　圭一",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec5 = ADSLW0120Rec (
         orgID = 1075,
         orgCode = "AHA",
         orgName = "吉田　卓哉",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec6 = ADSLW0120Rec (
        orgID = 1075,
        orgCode = "AHA",
        orgName = "吉田　卓哉",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec7 = ADSLW0120Rec (
        orgID = 1058,
        orgCode = "AGA",
        orgName = "伊藤　圭一",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec8 = ADSLW0120Rec (
         orgID = 1046,
         orgCode = "AFA",
         orgName = "柳沢　公晴",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec9 = ADSLW0120Rec (
         orgID = 1043,
         orgCode = "AEC",
         orgName = "澤田　健二",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

        val rec10 = ADSLW0120Rec (
         orgID = 1014,
         orgCode = "AAA",
         orgName = "吉田　宏明",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

      val bestgetrsp = List(rec1,rec2,rec3,rec4,rec5)
      val worstgetrsp = List(rec6,rec7,rec8,rec9,rec10)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }
    "No.3計画達成ランキング・本日・新店舗・経準高・ドロップダウン：全国・エリアタブ" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1, //期間種別：日単位
          srchTermFrom = 20050402,
          srchTermTo = 20050402,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          storeYearType = 3, //既存店
          srchValType  = 2, //出力区分：経準高
          srchOrgType =2 //エリアタブ
	  )

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 1057,
        orgCode = "BDA",
        orgName = "森越　大介",
        am = 100,
        planComp = 4.0,
        planSub = -2400,
        lastComp = 0.0,
        lastSub = 100,
        profit = 90,
        profitPlanComp = 9.0,
        profitPlanSub = -910,
        profitLastComp = 0.0,
        profitLastSub = 90,
        profitRt = 90.0,
        profitRtPlanSub = 50.0,
        profitRtLastSub = 90.0)

      val rec2 = ADSLW0120Rec (
        orgID = 1057,
        orgCode = "BDA",
        orgName = "森越　大介",
        am = 100,
        planComp = 4.0,
        planSub = -2400,
        lastComp = 0.0,
        lastSub = 100,
        profit = 90,
        profitPlanComp = 9.0,
        profitPlanSub = -910,
        profitLastComp = 0.0,
        profitLastSub = 90,
        profitRt = 90.0,
        profitRtPlanSub = 50.0,
        profitRtLastSub = 90.0)


      val bestgetrsp = List(rec1)
      val worstgetrsp = List(rec2)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.4全国全店舗・計画達成ランキング・本日・経準高・ドロップダウン：全国・エリアタブ" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0120GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20050402,
          srchTermTo = 20050402,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          storeYearType = 1,//全店
          srchValType  = 2, //経準高
          srchOrgType = 2)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0120Rec (
        orgID = 1014,
        orgCode = "AAA",
        orgName = "吉田　宏明",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec2 = ADSLW0120Rec (
        orgID = 1043,
        orgCode = "AEC",
        orgName = "澤田　健二",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec3 = ADSLW0120Rec (
         orgID = 1046,
         orgCode = "AFA",
         orgName = "柳沢　公晴",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec4 = ADSLW0120Rec (
         orgID = 1058,
         orgCode = "AGA",
         orgName = "伊藤　圭一",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec5 = ADSLW0120Rec (
         orgID = 1075,
         orgCode = "AHA",
         orgName = "吉田　卓哉",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec6 = ADSLW0120Rec (
        orgID = 1057,
        orgCode = "BDA",
        orgName = "森越　大介",
        am = 100,
        planComp = 4.0,
        planSub = -2400,
        lastComp = 0.0,
        lastSub = 100,
        profit = 90,
        profitPlanComp = 9.0,
        profitPlanSub = -910,
        profitLastComp = 0.0,
        profitLastSub = 90,
        profitRt = 90.0,
        profitRtPlanSub = 50.0,
        profitRtLastSub = 90.0)

       val rec7 = ADSLW0120Rec (
        orgID = 1057,
        orgCode = "BDA",
        orgName = "森越　大介",
        am = 100,
        planComp = 4.0,
        planSub = -2400,
        lastComp = 0.0,
        lastSub = 100,
        profit = 90,
        profitPlanComp = 9.0,
        profitPlanSub = -910,
        profitLastComp = 0.0,
        profitLastSub = 90,
        profitRt = 90.0,
        profitRtPlanSub = 50.0,
        profitRtLastSub = 90.0)

      val rec8 = ADSLW0120Rec (
        orgID = 1075,
        orgCode = "AHA",
        orgName = "吉田　卓哉",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

      val rec9 = ADSLW0120Rec (
        orgID = 1058,
        orgCode = "AGA",
        orgName = "伊藤　圭一",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)

       val rec10 = ADSLW0120Rec (
         orgID = 1046,
         orgCode = "AFA",
         orgName = "柳沢　公晴",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

       val rec11 = ADSLW0120Rec (
         orgID = 1043,
         orgCode = "AEC",
         orgName = "澤田　健二",
         am = 110,
         planComp = 4.4,
         planSub = -2390,
         lastComp = 0.0,
         lastSub = 110,
         profit = 100,
         profitPlanComp = 10.0,
         profitPlanSub = -900,
         profitLastComp = 0.0,
         profitLastSub = 100,
         profitRt = 90.9,
         profitRtPlanSub = 50.9,
         profitRtLastSub = 90.9)

      val rec12 = ADSLW0120Rec (
        orgID = 1014,
        orgCode = "AAA",
        orgName = "吉田　宏明",
        am = 110,
        planComp = 4.4,
        planSub = -2390,
        lastComp = 0.0,
        lastSub = 110,
        profit = 100,
        profitPlanComp = 10.0,
        profitPlanSub = -900,
        profitLastComp = 0.0,
        profitLastSub = 100,
        profitRt = 90.9,
        profitRtPlanSub = 50.9,
        profitRtLastSub = 90.9)
      val bestgetrsp = List(rec1,rec2,rec3,rec4,rec5,rec6)
      val worstgetrsp = List(rec7,rec8,rec9,rec10,rec11, rec12)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0120GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0120GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => {
          bestgetrsp mustBe res_getrsp.rspBestList
          worstgetrsp mustBe res_getrsp.rspWorstList
        }
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }
  }
}

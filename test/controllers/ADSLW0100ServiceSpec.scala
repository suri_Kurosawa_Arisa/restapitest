package controllers

import org.scalatestplus.play._
import org.scalatest.BeforeAndAfter
import play.api.test._
import play.api.test.Helpers._
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning._
//import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import play.api.libs.json.Json
import play.api.libs.json._
import java.text.ParseException
import scala.concurrent._
import scala.sys.process._
import java.io.PrintWriter
import models._

import play.api.libs.ws.ning.NingWSClient
import scala.concurrent.ExecutionContext.Implicits.global



/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ADSLW0100ServiceSpec extends PlaySpec with BeforeAndAfter {
  before {
    val arg = ""
    val checkSuccess = "(Success)".r
    Process("/home/aokmdb/work/kurosawa/ADSLW0100/test1/insert.sh") !!
  }

  after {
    Process("/home/aokmdb/work/kurosawa/ADSLW0100/test1/delete.sh") !!
  }

  /*
   * ここより試験を記述
   * should は大項目
   * in は小項目と考える
   */
  "ADSLW0100" should {
    // 外のスコープで定義した内容は中のスコープで使えるので複数試験で使う定数はここで定義する。
//    val url = "http://10.1.2.34/system/api/ADSLW0100";
 //   val url = "http://localhost:8880/system/api/ADSLW0100";
      val url = "http://10.1.2.34/system/api/ADSLW0100";
    "No.1個人売ランキング・全国・ゾーンAJA" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 1)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 988,
        storeCode = "AF",
        storeName = "神奈川第二ゾーン",
        staffID = 21090,
        staffCode = "980040",
        staffName = "大木　渡",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.2個人売ランキング・全国・エリアAJA" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 2)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 222,
        storeCode = "0104",
        storeName = "横浜港北総本店",
        staffID = 20960,
        staffCode = "895034",
        staffName = "柳沢　公晴",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.3個人売ランキング・全国・総店長・店長" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 3)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 222,
        storeCode = "0104",
        storeName = "横浜港北総本店",
        staffID = 20961,
        staffCode = "972042",
        staffName = "太田　耕一郎",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.4個人売ランキング・全国・副店長" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 4)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 222,
        storeCode = "0104",
        storeName = "横浜港北総本店",
        staffID = 1584,
        staffCode = "023366",
        staffName = "渡辺　千恵子",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.5個人売ランキング・全国・一般" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 5)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 222,
        storeCode = "0104",
        storeName = "横浜港北総本店",
        staffID = 21501,
        staffCode = "991057",
        staffName = "田中　秀之",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

     "No.6個人売ランキング・全国・パートナー・アルバイト" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 6)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 235,
        storeCode = "0121",
        storeName = "すみれヶ丘店",
        staffID = 17036,
        staffCode = "083495",
        staffName = "祇園　美保",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }

    "No.7個人売ランキング・全国・SC社員" in  {
      // テストの実行単位

      // 試験条件
      // リクエスト内容
      val reqhead = AdProtoCommonReq(
        fileId = 0,
        forceUpdFlag = 0,
        opeTypeId = 4,
        recno= "",
        state = 0)

      val reqpage = AdProtoPageReq(
        start_record = 0,
        page_size = 8)

      val getreq = ADSLW0100GetReq(
          reqHead = reqhead,
          reqPage = reqpage,
          srchTermType = 1,
          srchTermFrom = 20200401,
          srchTermTo = 20200401,
          srchUnitID = 3,
          srchZoneID = 0,
          srchAreaID = 0,
          srchStaffPostGrp = 7)

      // レスポンス内容
      val rspHead = AdProtoCommonRes(
         status = 0,
         message = "",
         args = Seq[String](),
         exmessage = "",
         fieldMessages = Seq[String](),
         ope_iymd = 20200401,
         ope_week = 202001,
         uri = "",
         recno = "",
         state = 0,
         liveFlag = 0,
         updDate = 0,
         updTime = 0)

      val rspPage = AdProtoPageRes(
          curr_record = 0,
          total_record = 1,
          page_record = 1,
          page_size = 8,
          page_num = 1)

      val rec1 = ADSLW0100Rec (
        storeID = 222,
        storeCode = "0104",
        storeName = "横浜港北総本店",
        staffID = 19319,
        staffCode = "720015",
        staffName = "町田　豊隆",
        am = 3000,
        qyAvg = 1.00,
        heavyCqy = 1,
        heavyCqyPrice = 3000,
        heavyCprice = 0,
        mediumCqy = 0,
        mediumCqyPrice = 0,
        mediumCprice = 0)

      val getrsp = List(rec1)

      // テスト実施
      import akka.actor._
      import akka.stream._
      import play.api.libs.ws.ahc._

      implicit val system = ActorSystem()
      implicit val materializer = ActorMaterializer()
      val ws = AhcWSClient()
//      val wsclient = new NingWSClient()
      val res: Future[ADSLW0100GetRsp] =
        ws.url(url)
          .withHeaders("Content-Type" -> "application/json")
          .post(Json.toJson(getreq)).map {
        response => response.json.validate[ADSLW0100GetRsp]
        match {
          case JsSuccess(result, _) => result
          case JsError(error) => throw new ParseException(error.toString(), 0)
        }
      }
      res onComplete {
        case Success(res_getrsp) => getrsp mustBe res_getrsp.rspList
        case Failure(t) => println("time out")
      }
      // 待ち合わせ
      Await.result(res, Duration(120, SECONDS))
    }


  }

}

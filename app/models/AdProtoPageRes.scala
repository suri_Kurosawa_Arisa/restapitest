package models

import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._

case class AdProtoPageRes(
  curr_record: Int,
  total_record: Int,
  page_record: Int,
  page_size: Int,
  page_num: Int
)

object AdProtoPageRes{
  implicit def jsonWrites = Json.writes[AdProtoPageRes]
  implicit def jsonReads =Json.reads[AdProtoPageRes]
}


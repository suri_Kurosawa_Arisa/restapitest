package models

import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._
import models._


case class ADACW0010GetReq (
  reqHead: AdProtoCommonReq,
  reqPage: AdProtoPageReq,
  srchTermType: Int,
  srchTermFrom: Int,
  srchTermTo: Int,
  srchUnitID: Int,
  srchZoneID: Int,
  srchAreaID: Int,
  srchStoreID: Int
    ) {
}

object ADACW0010GetReq {
  implicit def jsonWrites = Json.writes[ADACW0010GetReq]
  implicit def jsonReads = Json.reads[ADACW0010GetReq]
}

case class ADACW0010Rec (
  days: Int,
  daysBase: Int,
  amReq: Int,
  amComp: Double,
  amSub: Int,
  bizProfitReq: Int,
  bizProfitComp: Double,
  bizProfitSub: Int,
  am: Int,
  amPlanComp: Double,
  amPlanSub: Int,
  profit: Int,
  profitPlanComp: Double,
  profitPlanSub: Int,
  bizProfit: Int,
  bizProfitPlanComp: Double,
  bizProfitPlanSub: Int
    ) {
}
object ADACW0010Rec {
  // 子素養マクロを先に読み込まなくてはいけない様なので object ADACW0010RecはADACW0010GetRspより前に定義する必要がある
  implicit def jsonWrites = Json.writes[ADACW0010Rec]
  implicit def jsonReads = Json.reads[ADACW0010Rec]
}

case class ADACW0010GetRsp (
  rspHead: AdProtoCommonRes,
  rspPage: AdProtoPageRes,
  rspList: Seq[ADACW0010Rec]
    ) {
}
object ADACW0010GetRsp {
  // FormatにADACW0010GetRspの変換方法だけ示しただけでは足りないため子要素であるADACW0010Recの変換方法をimport
  import ADACW0010Rec._
  implicit val ADACW0010getrspFormat: Format[ADACW0010GetRsp] = (
    (JsPath \ "rspHead").format[AdProtoCommonRes] and
    (JsPath \ "rspPage").format[AdProtoPageRes] and
    (JsPath \ "rspList").format[Seq[ADACW0010Rec]])(ADACW0010GetRsp.apply _, unlift(ADACW0010GetRsp.unapply))
}

package models
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._

case class AdProtoCommonRes(
  status: Int,
  message: String,
  args: Seq[String],
  exmessage: String,
  fieldMessages: Seq[String],
  ope_iymd: Int,
  ope_week: Int,
  uri: String,
  recno: String,
  state: Int,
  liveFlag: Int,
  updTime: Int,
  updDate: Int
)

object AdProtoCommonRes {
  implicit def jsonWrites = Json.writes[AdProtoCommonRes]
  implicit def jsonReads = Json.reads[AdProtoCommonRes]
}

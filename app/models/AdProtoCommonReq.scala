package models

import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._

case class AdProtoCommonReq (
  opeTypeId: Int,
  fileId: Int,
  forceUpdFlag: Int,
  recno: String,
  state: Int
)

object AdProtoCommonReq {
  implicit def jsonWrites = Json.writes[AdProtoCommonReq]
  implicit def jsonReads = Json.reads[AdProtoCommonReq]
}

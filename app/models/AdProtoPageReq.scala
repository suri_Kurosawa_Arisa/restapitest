package models
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._

case class AdProtoPageReq (
  start_record: Int,
  page_size: Int
)

object AdProtoPageReq {
  implicit def jsonWrites = Json.writes[AdProtoPageReq]
  implicit def jsonReads = Json.reads[AdProtoPageReq]
}

package models

import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._
import models._


case class ADSLW0100GetReq (
  reqHead: AdProtoCommonReq,
  reqPage: AdProtoPageReq,
  srchTermType: Int,
  srchTermFrom: Int,
  srchTermTo: Int,
  srchUnitID: Int,
  srchZoneID: Int,
  srchAreaID: Int,
  srchStaffPostGrp: Int
    ) {
}

object ADSLW0100GetReq {
  implicit def jsonWrites = Json.writes[ADSLW0100GetReq]
  implicit def jsonReads = Json.reads[ADSLW0100GetReq]
}

case class ADSLW0100Rec (
  storeID: Int,
  storeCode: String,
  storeName: String,
  staffID: Int,
  staffCode: String,
  staffName: String,
  am: Int,
  qyAvg: Double,
  heavyCqy: Int,
  heavyCqyPrice: Int,
  heavyCprice: Int,
  mediumCqy: Int,
  mediumCqyPrice: Int,
  mediumCprice: Int
    ) {
}
object ADSLW0100Rec {
  // 子素養マクロを先に読み込まなくてはいけない様なので object ADSLW0100RecはADSLW0100GetRspより前に定義する必要がある
  implicit def jsonWrites = Json.writes[ADSLW0100Rec]
  implicit def jsonReads = Json.reads[ADSLW0100Rec]
}

case class ADSLW0100GetRsp (
  rspHead: AdProtoCommonRes,
  rspPage: AdProtoPageRes,
  rspList: Seq[ADSLW0100Rec]
    ) {
}
object ADSLW0100GetRsp {
  // FormatにADSLW0100GetRspの変換方法だけ示しただけでは足りないため子要素であるADSLW0100Recの変換方法をimport
  import ADSLW0100Rec._
  implicit val adslw0100getrspFormat: Format[ADSLW0100GetRsp] = (
    (JsPath \ "rspHead").format[AdProtoCommonRes] and
    (JsPath \ "rspPage").format[AdProtoPageRes] and
    (JsPath \ "rspList").format[Seq[ADSLW0100Rec]])(ADSLW0100GetRsp.apply _, unlift(ADSLW0100GetRsp.unapply))
}

package models

import play.api.libs.json._
import play.api.libs.json.Json
import play.api.libs.functional.syntax._
import models._



case class ADSLW0120GetReq (
  reqHead: AdProtoCommonReq,
  reqPage: AdProtoPageReq,
  srchTermType: Int,
  srchTermFrom: Int,
  srchTermTo: Int,
  srchUnitID: Int,
  srchZoneID: Int,
  srchAreaID: Int,
  storeYearType: Int,
  srchValType: Int,
  srchOrgType: Int
    ) {
}

object ADSLW0120GetReq {
  implicit def jsonWrites = Json.writes[ADSLW0120GetReq]
  implicit def jsonReads = Json.reads[ADSLW0120GetReq]
}


case class ADSLW0120Rec (
  orgID: Int,
  orgCode: String,
  orgName: String,
  am: Int,
  planComp: Double,
  planSub: Int,
  lastComp: Double,
  lastSub: Int,
  profit: Int,
  profitPlanComp: Double,
  profitPlanSub: Int,
  profitLastComp: Double,
  profitLastSub: Int,
  profitRt: Double,
  profitRtPlanSub: Double,
  profitRtLastSub: Double
    ) {
}
object ADSLW0120Rec {
  // 子素養マクロを先に読み込まなくてはいけない様なので object ADSLW0120RecはADSLW0120GetRspより前に定義する必要がある
  implicit def jsonWrites = Json.writes[ADSLW0120Rec]
  implicit def jsonReads = Json.reads[ADSLW0120Rec]
}

case class ADSLW0120GetRsp (
  rspHead: AdProtoCommonRes,
  rspPage: AdProtoPageRes,
  rspBestList: Seq[ADSLW0120Rec],
  rspWorstList: Seq[ADSLW0120Rec]
    ) {
}
object ADSLW0120GetRsp {
  // FormatにADSLW0120GetRspの変換方法だけ示しただけでは足りないため子要素であるADSLW0120Recの変換方法をimport
  import ADSLW0120Rec._
  implicit val adslw0120getrspFormat: Format[ADSLW0120GetRsp] = (
    (JsPath \ "rspHead").format[AdProtoCommonRes] and
    (JsPath \ "rspPage").format[AdProtoPageRes] and
    (JsPath \ "rspBestList").format[Seq[ADSLW0120Rec]] and
    (JsPath \ "rspWorstList").format[Seq[ADSLW0120Rec]])(ADSLW0120GetRsp.apply _, unlift(ADSLW0120GetRsp.unapply))
}
